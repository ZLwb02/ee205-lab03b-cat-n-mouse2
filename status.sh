#//////////////////////////////////////////////////////////////////////////////////////////////////
#/// @author Zack Lown <zlown@hawaii.edu>
#/// @date 03 Feb 2022
#///
#/// Status.sh
#///
#//////////////////////////////////////////////////////////////////////////////////////////////////
echo "I am"
whoami
echo "The current working directory is"
pwd
echo "The system I am on is"
uname -o
echo "The Linux Version is:"
uname -r
echo "The Linux distribution is"
uname -v
echo "The system has been up for"
uptime
echo "The amount of disk space I'm using in KB is"
du -k ~ |tail -n 1



