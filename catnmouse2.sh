#////////////////////////////////////////////////////////////////////////////////
#/// Zack Lown    <zlown@hawaii.edu>
#/// 03 Feb 2022
#///
#/// Cat 'N Mouse 2
#///
#/////////////////////////////////////////////////////////////////////////////////

#!/bin/bash

DEFAULT_MAX_NUMBER=2048
THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}
THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))
aGuess=0



while ((aGuess != THE_NUMBER_IM_THINKING_OF))
do 
   echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
   read -p "Make a guess: " aGuess
   if ((aGuess < 1))
   then
      echo "You must enter a number that's >= 1"
   elif ((aGuess > THE_MAX_VALUE))
   then
      echo "You must enter a number that's <= $THE_MAX_VALUE"
   elif ((aGuess > THE_NUMBER_IM_THINKING_OF))
   then
      echo "No cat... the number I'm thinking of is smaller than $aGuess"
   elif ((aGuess < THE_NUMBER_IM_THINKING_OF))
   then
      echo "No cat... the number I'm thinking of is larger than $aGuess"
   fi
done
echo "You got me."
echo "  /\_/\  ("
echo " ( ^.^ ) _)"
echo "  )   ("
echo " ( | | )"
echo "(__d b__)"

